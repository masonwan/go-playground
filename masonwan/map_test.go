package masonwan

import (
	"testing"
	"fmt"
)

type Id int
type Person struct {
	name string
	age  Id
}

func TestMap_replace(t *testing.T) {
	m := make(map[Id]Person)
	m[1] = Person{
		name: "a",
		age:  1,
	}
	m[2] = Person{
		name: "b",
		age:  2,
	}
	m[2] = Person{
		name: "c",
		age:  3,
	}

	if m[1].name != "a" {
		t.Errorf("Want %s got %s", "a", m[1].name)
	}
	if m[2].name != "c" {
		t.Errorf("Want %s got %s", "c", m[1].name)
	}
}

func TestMap_miss(t *testing.T) {
	m := make(map[string]bool)
	m["foo"] = true

	if m["no such key"] {
		t.Errorf("Should not return true for a missing key")
	}
}

func TestMapLength(t *testing.T) {
	m := map[Id]Person{}
	m[1] = Person{
		name: "a",
		age:  1,
	}
	m[2] = Person{
		name: "b",
		age:  2,
	}
	m[2] = Person{
		name: "c",
		age:  3,
	}

	if len(m) != 2 {
		t.Errorf("Expect to have 2 items but got %d itmes. map is %s", len(m), fmt.Sprint(m))
	}
}
