package masonwan

import (
	"testing"
	"reflect"
)

func TestPanic_catch(t *testing.T) {
	defer func() {
		err := recover()
		if err == nil {
			t.Error("It should generate error")
			return
		}
		t.Logf("%v", err)
		t.Logf("the type is '%+v'", reflect.TypeOf(err))
	}()
	raiseIndexOutOfBoundError()
}

func TestPanic_insideDefer(t *testing.T) {
	defer func() {
		t.Logf("Got the 2nd error")
		err := recover()
		t.Logf("%v", err)
	}()
	defer func() {
		t.Logf("Got the 1st error")
		err := recover()
		t.Logf("%v", err)

		raiseDivideByZeroError()
	}()
	raiseIndexOutOfBoundError()
}

func raiseIndexOutOfBoundError() {
	//noinspection GoPreferNilSlice
	var nums = []int{}
	nums[0] = 0
}

func raiseDivideByZeroError() int {
	x := 2
	y := 0
	//noinspection GoDivisionByZero
	return x / y
}
