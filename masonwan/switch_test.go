package masonwan

import "testing"

func TestSwitch(t *testing.T) {
	x := 1
	var y int

	switch x {
	case 1:
		y = 1
	case 2:
		y = 2
	default:
		y = 3
	}

	if y != 1 {
		t.Errorf("switch does not work as expect, y = %d", y)
	}
}
