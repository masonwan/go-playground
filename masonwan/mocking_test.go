package masonwan

import (
	"testing"
	"fmt"
)

type Animal struct {
	name string
}
type Cat struct {
	*Animal
	clawCount int
}

func (cat Cat) say() string {
	return fmt.Sprintf("meow, my name is %s", cat.name)
}

func TestEmbedding_setValueOnParentType(t *testing.T) {
	const name = "foo"
	cat := Cat{
		Animal:    &Animal{name: name},
		clawCount: 8,
	}

	if cat.name != name {
		t.Errorf("got %v want %v", cat.name, name)
	}
}

type InterfaceA interface {
	name() string
}

type A struct {
}

func (a A) name() string {
	return "a"
}

func NewA() *A {
	return &A{}
}

type MockA struct{}

func (mockA MockA) name() string {
	return "mocked"
}

func TestMock_MockMethod(t *testing.T) {
	var a InterfaceA

	a = NewA()
	t.Logf(a.name())

	a = MockA{}
	t.Logf(a.name())
}
